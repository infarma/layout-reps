package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Trailer struct {
	TipoRegsitro               int32 `json:"TipoRegsitro"`
	NumeroPedido               int32 `json:"NumeroPedido"`
	QuantidadeLinhas           int32 `json:"QuantidadeLinhas"`
	QuantidadeItensAtendidos   int32 `json:"QuantidadeItensAtendidos"`
	QuantidadeItensNaoAtendida int32 `json:"QuantidadeItensNaoAtendida"`
}

func (t *Trailer) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTrailer

	err = posicaoParaValor.ReturnByType(&t.TipoRegsitro, "TipoRegsitro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeLinhas, "QuantidadeLinhas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeItensAtendidos, "QuantidadeItensAtendidos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeItensNaoAtendida, "QuantidadeItensNaoAtendida")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTrailer = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegsitro":               {0, 1, 0},
	"NumeroPedido":               {1, 13, 0},
	"QuantidadeLinhas":           {13, 18, 0},
	"QuantidadeItensAtendidos":   {18, 23, 0},
	"QuantidadeItensNaoAtendida": {23, 28, 0},
}
