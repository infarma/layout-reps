package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Header struct {
	TipoRegistro               int32  `json:"TipoRegistro"`
	CodigoCliente              string `json:"CodigoCliente"`
	NumeroPedido               string `json:"NumeroPedido"`
	DataPedido                 int32  `json:"DataPedido"`
	TipoCompra                 string `json:"TipoCompra"`
	TipoRetorno                string `json:"TipoRetorno"`
	ApontadorCondicaoComercial int32  `json:"ApontadorCondicaoComercial"`
	NumeroPedidoCliente        string `json:"NumeroPedidoCliente"`
}

func (h *Header) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHeader

	err = posicaoParaValor.ReturnByType(&h.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CodigoCliente, "CodigoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DataPedido, "DataPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.TipoCompra, "TipoCompra")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.TipoRetorno, "TipoRetorno")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.ApontadorCondicaoComercial, "ApontadorCondicaoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroPedidoCliente, "NumeroPedidoCliente")
	if err != nil {
		return err
	}

	return err
}

var PosicoesHeader = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":               {0, 1, 0},
	"CodigoCliente":              {1, 16, 0},
	"NumeroPedido":               {16, 28, 0},
	"DataPedido":                 {28, 36, 0},
	"TipoCompra":                 {36, 37, 0},
	"TipoRetorno":                {37, 38, 0},
	"ApontadorCondicaoComercial": {38, 43, 0},
	"NumeroPedidoCliente":        {43, 58, 0},
}
