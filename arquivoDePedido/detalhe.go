package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Detalhe struct {
	TipoRegistro       int32   `json:"TipoRegistro"`
	NumeroPedido       string  `json:"NumeroPedido"`
	CodigoPedido       int64   `json:"CodigoPedido"`
	Quantidade         int32   `json:"Quantidade"`
	Desconto           float32 `json:"Desconto"`
	Prazo              int32   `json:"Prazo"`
	UtilizacaoDesconto string  `json:"UtilizacaoDesconto"`
	UtilizacaoPrazo    string  `json:"UtilizacaoPrazo"`
}

func (d *Detalhe) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDetalhe

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoPedido, "CodigoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Desconto, "Desconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Prazo, "Prazo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.UtilizacaoDesconto, "UtilizacaoDesconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.UtilizacaoPrazo, "UtilizacaoPrazo")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDetalhe = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":       {0, 1, 0},
	"NumeroPedido":       {1, 13, 0},
	"CodigoPedido":       {13, 26, 0},
	"Quantidade":         {26, 31, 0},
	"Desconto":           {31, 36, 2},
	"Prazo":              {36, 39, 0},
	"UtilizacaoDesconto": {39, 40, 0},
	"UtilizacaoPrazo":    {40, 41, 0},
}
