package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Trailer struct {
	TipoRegsitro       int32  `json:"TipoRegsitro"`
	NumeroPedido       string `json:"NumeroPedido"`
	QuantidadeItens    int32  `json:"QuantidadeItens"`
	QuantidadeUnidades int64  `json:"QuantidadeUnidades"`
}

func (t *Trailer) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTrailer

	err = posicaoParaValor.ReturnByType(&t.TipoRegsitro, "TipoRegsitro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeItens, "QuantidadeItens")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeUnidades, "QuantidadeUnidades")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTrailer = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegsitro":       {0, 1, 0},
	"NumeroPedido":       {1, 13, 0},
	"QuantidadeItens":    {13, 18, 0},
	"QuantidadeUnidades": {18, 28, 0},
}
